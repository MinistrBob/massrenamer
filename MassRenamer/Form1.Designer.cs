﻿namespace MassRenamer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btStart = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btFolder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btStart
            // 
            this.btStart.AutoSize = true;
            this.btStart.Location = new System.Drawing.Point(669, 44);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(75, 30);
            this.btStart.TabIndex = 0;
            this.btStart.Text = "Start";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(45, 91);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(699, 26);
            this.tbPath.TabIndex = 1;
            this.tbPath.Text = "d:\\!SAVE\\777\\";
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(45, 124);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbLog.Size = new System.Drawing.Size(699, 303);
            this.tbLog.TabIndex = 2;
            // 
            // btFolder
            // 
            this.btFolder.AutoSize = true;
            this.btFolder.Location = new System.Drawing.Point(45, 44);
            this.btFolder.Name = "btFolder";
            this.btFolder.Size = new System.Drawing.Size(75, 30);
            this.btFolder.TabIndex = 3;
            this.btFolder.Text = "Folder";
            this.btFolder.UseVisualStyleBackColor = true;
            this.btFolder.Click += new System.EventHandler(this.btFolder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 439);
            this.Controls.Add(this.btFolder);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.btStart);
            this.Name = "Form1";
            this.Text = "MassRenamer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btFolder;
    }
}

