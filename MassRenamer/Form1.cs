﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MassRenamer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tbLog.Clear();
            string curDir;
            curDir = Directory.GetCurrentDirectory();
            IEnumerable<string> jpgFilesInDir;
            List<string> files = new List<string>();

            if (tbPath.Text.Length > 0)
            {
                if (Directory.Exists(tbPath.Text))
                {
                    jpgFilesInDir = Directory.EnumerateFiles(tbPath.Text, "*.jpg");  //получаем список файлов в указаной директории
                    files.AddRange(jpgFilesInDir);
                    jpgFilesInDir = Directory.EnumerateFiles(tbPath.Text, "*.jpeg");
                    files.AddRange(jpgFilesInDir);
                }
                else
                {
                    tbLog.AppendText("Указанного пути не сущестует\n");
                    return;
                }
            }
            else
            {
                jpgFilesInDir = Directory.EnumerateFiles(curDir, "*.jpg");   //получаем список файлов в текущей директории
            }
            FileInfo fInf;
            foreach (string currentFile in jpgFilesInDir)   //проходим по всем файлам
            {
                fInf = new FileInfo(currentFile);
                tbLog.AppendText(String.Format("Обрабатываем файл: {0}\n", fInf.Name));
                ReadExifInfo rex = new ReadExifInfo(currentFile, tbLog);
                try
                {
                    if (rex.CreateTime.Date.Year > 1980)
                    {
                        tbLog.AppendText(String.Format("Дата съемки: {0}\n", rex.CreateTime));
                        if (fInf.LastWriteTime != rex.CreateTime)
                        {
                            tbLog.AppendText("Устанавливаем новое время редактирования файла.\n");
                            fInf.LastWriteTime = rex.CreateTime;
                        }
                        else
                        {
                            tbLog.AppendText("Даты совпадают, изменение не требуется.\n");
                        }
                    }
                    else
                    {
                        tbLog.AppendText("Некорректная дата съемки.\n");
                    }
                }
                catch (Exception excp)
                {
                    tbLog.AppendText(excp.Message);
                }
            }

        }

        private void btFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
