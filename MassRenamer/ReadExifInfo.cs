﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MassRenamer
{
    class ReadExifInfo
    {    //для чтения EXIF информации из файла
        private DateTime creationTime;
        private FileStream Foto;
        private BitmapMetadata TmpImgEXIF;

        public ReadExifInfo(string fileName, System.Windows.Forms.TextBox tbLog)
        { //конструктор
            try
            {
                Foto = File.Open(fileName, FileMode.Open, FileAccess.Read); // открыли файл по адресу fileName для чтения
            }
            catch (Exception)
            {
                tbLog.AppendText("Невозможно открыть файл\n");
            }
            try
            {
                JpegBitmapDecoder decoder = new JpegBitmapDecoder(Foto, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.Default); //"распаковали" снимок и создали объект decoder
                TmpImgEXIF = (BitmapMetadata)decoder.Frames[0].Metadata.Clone(); //считали и сохранили метаданные
                creationTime = Convert.ToDateTime(TmpImgEXIF.DateTaken);    //в переменную дату съемки
                Foto.Close();
            }
            catch (Exception e)
            {
                tbLog.AppendText(e.Message+ "\n");
                tbLog.AppendText("Невозможно прочитать EXIF информацию.\n");
                Foto.Close();
            }
        }

        public DateTime CreateTime
        {    //возвращаем дату и время съемки
            get { return creationTime; }
        }
    }
}
